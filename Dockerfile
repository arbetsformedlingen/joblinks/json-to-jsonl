FROM docker.io/library/debian:stable-20240211-slim

WORKDIR /data

COPY json2jsonl.jq /usr/local/bin/

RUN apt-get -y update &&\
    apt-get -y install jq &&\
    apt-get -y clean && apt-get -y autoremove &&\
    chmod a+rx /usr/local/bin/json2jsonl.jq

ENTRYPOINT ["/usr/local/bin/json2jsonl.jq"]
